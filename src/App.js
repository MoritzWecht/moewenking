import React from 'react';
import Header from './Header/Header';
import Rooms from './Rooms/Rooms';

import './App.scss';

function App() {
  return (
    <div className="App">
      <Header />
      <Rooms />
    </div>
  );
}

export default App;

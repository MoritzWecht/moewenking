import React from 'react';

import './Header.scss';

export default class Header extends React.Component {

    render() {
        return (
            <div className="header">
                <h1>Möwenking</h1>
            </div>
        );
    }
}
import React from 'react';
import Device from './Device/Device';
import Icon from '@material-ui/core/Icon';

const devices = require('../../utils/devices');

export default class Room extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <div className="room">
                <div className="room--icon"><Icon>{this.props.icon}</Icon></div>
                <div className="room--name">{this.props.name}</div>
                <div className="devices">
                    {
                        devices.map((device, id) =>
                            device.room === this.props.id ?
                                <Device name={device.name} id={device.id} key={id} />
                                :
                                null
                        )
                    }
                </div>
            </div>
        );
    }
}
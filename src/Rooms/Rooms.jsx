import React from 'react';
import Room from './Room/Room';

import './Rooms.scss';

const rooms = require('../utils/rooms');

export default class Rooms extends React.Component {

    render() {
        return (
            <div className="rooms">
                {
                    rooms.map((room, id) =>
                        <Room name={room.name} icon={room.icon} id={room.id} key={id} />
                    )
                }
            </div>
        );
    }
}